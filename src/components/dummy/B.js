import { useState } from "react";
import ReactKeypress from "../modules/ReactKeypress";

const B = (props) => {
  const { setToggleStates, toggleStates } = props;
  console.log(props);

  const toggleOnClick = () => {
    setToggleStates(
      toggleStates.map((elem, idx) => {
        if (idx === 0) {
          return !elem;
        }
        return elem;
      })
    );
  };

  return (
    <ReactKeypress.Item>
      <div onClick={toggleOnClick}>
        B {toggleStates[1] ? "active" : "inactive"}
      </div>
    </ReactKeypress.Item>
  );
};

export default B;
