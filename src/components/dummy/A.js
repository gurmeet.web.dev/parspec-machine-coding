import { useState } from "react";
import ReactKeypress from "../modules/ReactKeypress";

const A = (props) => {
  const { setToggleStates, toggleStates } = props;
//   console.log(props);

  const toggleOnClick = () => {
    setToggleStates(
      toggleStates.map((elem, idx) => {
        if (idx === 0) {
          return !elem;
        }
        return elem;
      })
    );
  };

  return (
    <ReactKeypress.Item>
      <div onClick={toggleOnClick}>
        A {toggleStates[0] ? "active" : "inactive"}
      </div>
    </ReactKeypress.Item>
  );
};

export default A;
