import { useEffect } from "react";

const listener = new window.keypress.Listener();

const Item = (props) => {
  const { children } = props;
  return <>{children}</>;
};

const ReactKeypress = (props) => {
  const {
    combo = "shift s",
    callback = () => {
      console.log("combo was pressed");
    },
    children,
  } = props;

  useEffect(() => {
    listener.simple_combo(combo, callback);
  }, []);

  return <>{children}</>;
};

ReactKeypress.Item = Item;

export default ReactKeypress;
