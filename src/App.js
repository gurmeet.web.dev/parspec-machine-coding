import "./App.css";
import ReactKeypress from "./components/modules/ReactKeypress";
import A from "./components/dummy/A";
import B from "./components/dummy/B";
import { useEffect, useRef, useState, useCallback } from "react";

function App() {
  const [toggleStates, setToggleStates] = useState([true, true, true, true]);

  const modifyToggleStates = () => {
    const updatedState = toggleStates.map((state) => !state);
    setToggleStates(updatedState);
  };

  // useEffect(() => {
  //   // console.log(toggleStates);
  // }, [toggleStates]);

  return (
    <div className="App">
      <header className="App-header">
        <ReactKeypress combo="shift s" callback={modifyToggleStates}>
          <A toggleStates={toggleStates} setToggleStates={setToggleStates} />
          {/* <B toggleStates={toggleStates} setToggleStates={setToggleStates} /> */}
        </ReactKeypress>
      </header>
    </div>
  );
}

export default App;
